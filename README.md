## What is this?

Separating the frontend and backend of a website is an architectural pattern where the UI components (Javascript, CSS,
etc.) are developed, bundled, deployed, and delivered separately from the service code (e.g. SpringBoot, Java, Hibernate, 
etc). All of the display-related business logic is shifted to the frontend in this architectural pattern and the 
server-side business logic slimmed down considerably as a result. Backend code is exposed to frontend code through a 
RESTful web interface; this interface creates a light coupling between the two components that is easily extensible to 
other frontend applications. 

This repository contains a proof of concept that demonstrates this architectural pattern. An example Vue app is created
and built using Nuxt, the output of the Nuxt build is uploaded to an AWS S3 bucket and exposed via AWS Cloudfront. The
Vue application is integrated with existing MAUI code via a lightweight `taglib` that mounts the Vue app to a JSP page
that contains the standard MAUI UI layout. 

## Why?

The primary goal is to separate logical domains into "[bounded contexts](https://martinfowler.com/bliki/BoundedContext.html)" 
that can be independently developed and deployed. Composing the larger MAUI application into bounded contexts will give
developers more ownership, autonomy, and velocity. 

Performing this split now will enable teams to integrate with newer technologies (i.e. NPM, Vue, ais-components) at their
own pace. Integrating Vue with JSP enables the organization to upgrade webpages in a piecemeal way where the harder
aspects (i.e. people inspector, tabs, navigation) can be performed independently and at a later date, when the organization
is ready to do so. 

## How does this work?

Developers can use Vue and Nuxt to develop frontend applications independent of MAUI. In a normal Nuxt application, the
user would make a request to fetch `index.html` which returns a small html page containing various Javascript and CSS
assets that mount and display the Vue app. In this implementation, the `index.html` page is retrieved and parsed by a
MAUI webpage and the Javascript and CSS assets are loaded such that the Vue app is mounted within the MAUI page. 

![screenshot](./docs/MauiScreenShot.png)

In the example above the outlined red section contains the Vue App, everything else is standard Maui JSP. This 
implementation enables developers to build the "body" of a webpage in Vue without having to duplicate common MAUI 
components like the Person Inspector, Tabs, and Navigation panes. 

## Why mount Vue to MAUI?

Mounting a Vue app to Maui allows us to implement Maui pages in Vue using ais-components while taking advantage of common
Maui constructs that haven't been implemented in Vue, such as the Person Inspector, Tabs, left-hand navigation,
and much more. Mounting to MAUI delays the burden of needing to recreate these common components in ais-components (or
another library). It enables developers and teams to upgrade pieces of MAUI slowly, overtime, as dictiated by their 
teams capacity and velocity.

## How does a Vue app communicate with MAUI?

Vue app can communicate with MAUI's backend by making normal HTTP requests. There is no need for extra 
authentication/authorization because the requests will contain MAUI's normal authentication cookies. 

There's currently a limitation where Vue apps must use `.page` endpoints that return a `StreamingResolution` because 
MAUI's API only authorizes requests with `Bearer` or `Basic` Authorization headers. However, support can be added to the
API to [accept authentication cookies](https://gitlab.com/uiowa-ais/java/maui/-/commit/5f0aa57d2583eb16c86798dc0476d4d8a51514e9) 
which would enable Vue apps to call MAUI's API directly. If we  enforce that all Vue apps call MAUI's API -- instead 
of `.page` endpoints -- then we will further decouple frontend and backend codebases while also moving MAUI towards a 
more modern architecture. 

There are instances where MAUI must pass information to the Vue app. This is accomplished via Vue's routing mechanisms 
that are set up by Nuxt.

## How does routing work?

Nuxt will automatically generate routes based on the file structure in the `pages` directory. This can be useful, however
it has its frustrations, particularly when you have a path like `/maui/person/inspect/billing/documents.page` which
requires a folder structure for each path component, including a `documents.page` folder. 

The frustrations can be resolved by aliasing routes using Nuxt constructs. As an example, the
`/maui/person/inspect/billing/documents.page` path can be routed to `documents/documents.vue` by defining page metadata
in the Vue file: 

```
<script setup>

definePageMeta({
  alias: ["/maui/person/inspect/billing/documents.page"]
})

</script>
```

## How do you mount a Vue app to MAUI?

This is where things get...funky. Nuxt produces a few important artifacts: 

1. A JavaScript file containing the entry point to the Vue application
2. A snippet of JavaScript that contains configuration information
3. An index.html file containing a `script` tag to download the JavaScript file and a `script` tag to execute the configuration snippet

In theory, all you have to do is import the JavaScript file and execute the configuration snippet. However...there is no
easy way (that I found) to determine what those are before or after building the application. Since the `index.html` file
is always in the same location, I was able to parse the HTML to retrieve the important bits and render those bits in
the browser. This doesn't feel great and I'm sure there is a better way to do it, but this is all I was able to do given the minimal
effort I put in to solving the problem. 

I've wrapped all of this logic in a `taglib` so it can be shared and abstracted ([code](https://gitlab.com/uiowa-ais/java/maui/-/blob/d3ab1ff3894687cc3918db84bf0faddb4c4f66e1/web/WEB-INF/tags/billing/nuxt-app.tag)).
Otherwise, the actual integration is quite simple since all you need to do is import the tag ([code](https://gitlab.com/uiowa-ais/java/maui/-/blob/d3ab1ff3894687cc3918db84bf0faddb4c4f66e1/web/WEB-INF/pages/billing/accounts/shared/billing-account-documents.jsp)).

## How's the development experience?

Inside the `taglib` mentioned in the previous section there is some logic to determine if sources should be retrieved
from `localhost:3000` or a production/staging endpoint. When developing using sources from `localhost:3000` you get
all the benefits of Nuxt/NPM including hot reloading. 

There are, however, some issues with development, particularly related to CSS. MAUI uses Tailwind and a few other
CSS files for branding, these files (particularly Tailwind) are not guaranteed to have the same version or same styling
as the same assets imported in the Vue app. If there is any clashes then the browser will opt to use the Vue app's styling
over MAUI's.  This can result in the styling of MAUI to change when the Vue app is mounted. 

This CSS issue can be fixed -- I believe -- by bundling MAUI's CSS files into an NPM package and importing that package
as a dev dependency in the Vue app. Developers will get the benefit of auto-complete and intellisense during development,
but the actual assets will not be included with the built artifacts which means MAUI's styling will not be overridden.

## How to deploy?

Static Vue apps can be deployed by loading the `.output` directory to a CDN. MAUI can load these assets to a CDN and
mount the Vue app to the webpage. 

An example is provided in the `cdk` directory. The `lib/billing-stack.ts` file contains am AWS CDK application that 
creates an S3 bucket and CloudFront distribution that acts as an edge cache for the built Vue app. Nuxt's `.output` 
directory is uploaded to the S3 bucket during deployment of the CDK application and the CloudFront distribution's cache
is invalidated. 

The CloudFront distribution is added to the `nuxt.config.ts` file so that the application references the CDN when fetching
assets. 

```
app:{
   baseURL: '/',
   cdnURL: 'https://d353p524qskz4a.cloudfront.net'
}
```

The CloudFront distribution can be added to MAUI's configuration files so that the `taglib` can reference the correct
`index.html` file to mount the Vue app. CloudFront distributions can be created per environment (i.e. billing-test, dev,
test, production) to enable testing and user acceptance before deployment.