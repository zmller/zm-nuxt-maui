// https://v3.nuxtjs.org/api/configuration/nuxt.config
import { defineNuxtConfig } from 'nuxt/config'


export default defineNuxtConfig({
    ssr: false,
    app:{
       baseURL: '/',
       cdnURL: 'http://localhost:3000'
       // cdnURL: 'https://d353p524qskz4a.cloudfront.net'
    },
    components: [
        '~/components/documents',
        '~/components'
    ],
    modules: [
        '@ais-public/ais-nuxt',
        '@pinia/nuxt'
    ],
    css: [
        "~/assets/css/tailwind.css",
    ],
    aisNuxt: {
        server:{
            devServerPort:8080
        }
    }
})
