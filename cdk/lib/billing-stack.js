"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UIBillingExample = void 0;
const cdk = require("aws-cdk-lib");
const aws_s3_1 = require("aws-cdk-lib/aws-s3");
const path = require("path");
const aws_s3_deployment_1 = require("aws-cdk-lib/aws-s3-deployment");
const aws_cloudfront_1 = require("aws-cdk-lib/aws-cloudfront");
const aws_cloudfront_origins_1 = require("aws-cdk-lib/aws-cloudfront-origins");
class UIBillingExample extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        const corsRule = {
            allowedMethods: [aws_s3_1.HttpMethods.GET, aws_s3_1.HttpMethods.HEAD],
            allowedOrigins: ['*'],
            // the properties below are optional
            allowedHeaders: ['*'],
            exposedHeaders: ['ETag'],
            id: 'cors-policy-id'
        };
        const bucket = new aws_s3_1.Bucket(this, 'UIBillingBucket', {
            accessControl: aws_s3_1.BucketAccessControl.PRIVATE,
            cors: [corsRule]
        });
        const originAccessIdentity = new aws_cloudfront_1.OriginAccessIdentity(this, 'OriginAccessIdentity');
        bucket.grantRead(originAccessIdentity);
        const cachePolicy = new aws_cloudfront_1.CachePolicy(this, "CachePolicy", {
            headerBehavior: aws_cloudfront_1.CacheHeaderBehavior.allowList("Origin", "Access-Control-Request-Method", "Access-Control-Request-Headers"),
        });
        const distribution = new aws_cloudfront_1.Distribution(this, 'UIBillingDistribution', {
            defaultRootObject: 'index.html',
            defaultBehavior: {
                origin: new aws_cloudfront_origins_1.S3Origin(bucket, { originAccessIdentity }),
                responseHeadersPolicy: aws_cloudfront_1.ResponseHeadersPolicy.CORS_ALLOW_ALL_ORIGINS,
                cachePolicy: cachePolicy
            },
        });
        new aws_s3_deployment_1.BucketDeployment(this, 'BucketDeployment', {
            destinationBucket: bucket,
            sources: [aws_s3_deployment_1.Source.asset(path.resolve(__dirname, '../../.output/public'))],
            distribution,
            distributionPaths: ['/index.html']
        });
    }
}
exports.UIBillingExample = UIBillingExample;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmlsbGluZy1zdGFjay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImJpbGxpbmctc3RhY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsbUNBQW1DO0FBRW5DLCtDQUFzRjtBQUN0Riw2QkFBNkI7QUFDN0IscUVBQXVFO0FBQ3ZFLCtEQU1vQztBQUNwQywrRUFBNEQ7QUFFNUQsTUFBYSxnQkFBaUIsU0FBUSxHQUFHLENBQUMsS0FBSztJQUM3QyxZQUFZLEtBQWdCLEVBQUUsRUFBVSxFQUFFLEtBQXNCO1FBQzlELEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXhCLE1BQU0sUUFBUSxHQUFhO1lBQ3pCLGNBQWMsRUFBRSxDQUFDLG9CQUFXLENBQUMsR0FBRyxFQUFFLG9CQUFXLENBQUMsSUFBSSxDQUFDO1lBQ25ELGNBQWMsRUFBRSxDQUFDLEdBQUcsQ0FBQztZQUVyQixvQ0FBb0M7WUFDcEMsY0FBYyxFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3JCLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQztZQUN4QixFQUFFLEVBQUUsZ0JBQWdCO1NBQ3JCLENBQUM7UUFHRixNQUFNLE1BQU0sR0FBRyxJQUFJLGVBQU0sQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUU7WUFDakQsYUFBYSxFQUFFLDRCQUFtQixDQUFDLE9BQU87WUFDMUMsSUFBSSxFQUFFLENBQUMsUUFBUSxDQUFDO1NBQ2pCLENBQUMsQ0FBQTtRQUVGLE1BQU0sb0JBQW9CLEdBQUcsSUFBSSxxQ0FBb0IsQ0FBQyxJQUFJLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUNwRixNQUFNLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFFdkMsTUFBTSxXQUFXLEdBQUcsSUFBSSw0QkFBVyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUU7WUFDdkQsY0FBYyxFQUFFLG9DQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsK0JBQStCLEVBQUUsZ0NBQWdDLENBQUM7U0FDM0gsQ0FBQyxDQUFBO1FBRUYsTUFBTSxZQUFZLEdBQUcsSUFBSSw2QkFBWSxDQUFDLElBQUksRUFBRSx1QkFBdUIsRUFBRTtZQUNuRSxpQkFBaUIsRUFBRSxZQUFZO1lBQy9CLGVBQWUsRUFBRTtnQkFDZixNQUFNLEVBQUUsSUFBSSxpQ0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFDLG9CQUFvQixFQUFDLENBQUM7Z0JBQ3BELHFCQUFxQixFQUFFLHNDQUFxQixDQUFDLHNCQUFzQjtnQkFDbkUsV0FBVyxFQUFFLFdBQVc7YUFDekI7U0FFRixDQUFDLENBQUE7UUFFRixJQUFJLG9DQUFnQixDQUFDLElBQUksRUFBRSxrQkFBa0IsRUFBRTtZQUM3QyxpQkFBaUIsRUFBRSxNQUFNO1lBQ3pCLE9BQU8sRUFBRSxDQUFDLDBCQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDLENBQUMsQ0FBQztZQUN4RSxZQUFZO1lBQ1osaUJBQWlCLEVBQUUsQ0FBQyxhQUFhLENBQUM7U0FDbkMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztDQUNGO0FBNUNELDRDQTRDQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGNkayBmcm9tICdhd3MtY2RrLWxpYic7XG5pbXBvcnQgeyBDb25zdHJ1Y3QgfSBmcm9tICdjb25zdHJ1Y3RzJztcbmltcG9ydCB7QnVja2V0LCBCdWNrZXRBY2Nlc3NDb250cm9sLCBDb3JzUnVsZSwgSHR0cE1ldGhvZHN9IGZyb20gXCJhd3MtY2RrLWxpYi9hd3MtczNcIjtcbmltcG9ydCAqIGFzIHBhdGggZnJvbSBcInBhdGhcIjtcbmltcG9ydCB7QnVja2V0RGVwbG95bWVudCwgU291cmNlfSBmcm9tIFwiYXdzLWNkay1saWIvYXdzLXMzLWRlcGxveW1lbnRcIjtcbmltcG9ydCB7XG4gIENhY2hlSGVhZGVyQmVoYXZpb3IsXG4gIENhY2hlUG9saWN5LFxuICBEaXN0cmlidXRpb24sXG4gIE9yaWdpbkFjY2Vzc0lkZW50aXR5LFxuICBSZXNwb25zZUhlYWRlcnNQb2xpY3lcbn0gZnJvbSBcImF3cy1jZGstbGliL2F3cy1jbG91ZGZyb250XCI7XG5pbXBvcnQge1MzT3JpZ2lufSBmcm9tIFwiYXdzLWNkay1saWIvYXdzLWNsb3VkZnJvbnQtb3JpZ2luc1wiO1xuXG5leHBvcnQgY2xhc3MgVUlCaWxsaW5nRXhhbXBsZSBleHRlbmRzIGNkay5TdGFjayB7XG4gIGNvbnN0cnVjdG9yKHNjb3BlOiBDb25zdHJ1Y3QsIGlkOiBzdHJpbmcsIHByb3BzPzogY2RrLlN0YWNrUHJvcHMpIHtcbiAgICBzdXBlcihzY29wZSwgaWQsIHByb3BzKTtcblxuICAgIGNvbnN0IGNvcnNSdWxlOiBDb3JzUnVsZSA9IHtcbiAgICAgIGFsbG93ZWRNZXRob2RzOiBbSHR0cE1ldGhvZHMuR0VULCBIdHRwTWV0aG9kcy5IRUFEXSxcbiAgICAgIGFsbG93ZWRPcmlnaW5zOiBbJyonXSxcblxuICAgICAgLy8gdGhlIHByb3BlcnRpZXMgYmVsb3cgYXJlIG9wdGlvbmFsXG4gICAgICBhbGxvd2VkSGVhZGVyczogWycqJ10sXG4gICAgICBleHBvc2VkSGVhZGVyczogWydFVGFnJ10sXG4gICAgICBpZDogJ2NvcnMtcG9saWN5LWlkJ1xuICAgIH07XG5cblxuICAgIGNvbnN0IGJ1Y2tldCA9IG5ldyBCdWNrZXQodGhpcywgJ1VJQmlsbGluZ0J1Y2tldCcsIHtcbiAgICAgIGFjY2Vzc0NvbnRyb2w6IEJ1Y2tldEFjY2Vzc0NvbnRyb2wuUFJJVkFURSxcbiAgICAgIGNvcnM6IFtjb3JzUnVsZV1cbiAgICB9KVxuXG4gICAgY29uc3Qgb3JpZ2luQWNjZXNzSWRlbnRpdHkgPSBuZXcgT3JpZ2luQWNjZXNzSWRlbnRpdHkodGhpcywgJ09yaWdpbkFjY2Vzc0lkZW50aXR5Jyk7XG4gICAgYnVja2V0LmdyYW50UmVhZChvcmlnaW5BY2Nlc3NJZGVudGl0eSk7XG5cbiAgICBjb25zdCBjYWNoZVBvbGljeSA9IG5ldyBDYWNoZVBvbGljeSh0aGlzLCBcIkNhY2hlUG9saWN5XCIsIHtcbiAgICAgIGhlYWRlckJlaGF2aW9yOiBDYWNoZUhlYWRlckJlaGF2aW9yLmFsbG93TGlzdChcIk9yaWdpblwiLCBcIkFjY2Vzcy1Db250cm9sLVJlcXVlc3QtTWV0aG9kXCIsIFwiQWNjZXNzLUNvbnRyb2wtUmVxdWVzdC1IZWFkZXJzXCIpLFxuICAgIH0pXG5cbiAgICBjb25zdCBkaXN0cmlidXRpb24gPSBuZXcgRGlzdHJpYnV0aW9uKHRoaXMsICdVSUJpbGxpbmdEaXN0cmlidXRpb24nLCB7XG4gICAgICBkZWZhdWx0Um9vdE9iamVjdDogJ2luZGV4Lmh0bWwnLFxuICAgICAgZGVmYXVsdEJlaGF2aW9yOiB7XG4gICAgICAgIG9yaWdpbjogbmV3IFMzT3JpZ2luKGJ1Y2tldCwge29yaWdpbkFjY2Vzc0lkZW50aXR5fSksXG4gICAgICAgIHJlc3BvbnNlSGVhZGVyc1BvbGljeTogUmVzcG9uc2VIZWFkZXJzUG9saWN5LkNPUlNfQUxMT1dfQUxMX09SSUdJTlMsXG4gICAgICAgIGNhY2hlUG9saWN5OiBjYWNoZVBvbGljeVxuICAgICAgfSxcblxuICAgIH0pXG5cbiAgICBuZXcgQnVja2V0RGVwbG95bWVudCh0aGlzLCAnQnVja2V0RGVwbG95bWVudCcsIHtcbiAgICAgIGRlc3RpbmF0aW9uQnVja2V0OiBidWNrZXQsXG4gICAgICBzb3VyY2VzOiBbU291cmNlLmFzc2V0KHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuLi8uLi8ub3V0cHV0L3B1YmxpYycpKV0sXG4gICAgICBkaXN0cmlidXRpb24sXG4gICAgICBkaXN0cmlidXRpb25QYXRoczogWycvaW5kZXguaHRtbCddXG4gICAgfSlcbiAgfVxufVxuIl19